<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR .'functions.php';

function thCrawler($type = 'json')
{
    $uniCats = array(
    'สถาบันอุดมศึกษาของรัฐ',
    'สถาบันอุดมศึกษาในกำกับของรัฐ',
    'สถาบันอุดมศึกษาเอกชน',
    'สถาบันอุดมศึกษาอิสระ'
    );
    $subCats = array(
    'สถาบันอุดมศึกษา',
    'มหาวิทยาลัยราชภัฏ',
    'มหาวิทยาลัยเทคโนโลยีราชมงคล',
    'สถาบันวิทยาลัยชุมชน',
    'สถาบันการอาชีวศึกษา',
    'สถาบันการศึกษาของทหารและตำรวจ',
    'สถาบันการศึกษานอกสังกัดกระทรวงศึกษาธิการ',
    'มหาวิทยาลัย',
    'สถาบัน',
    'วิทยาลัย',
    'สถาบันอุดมศึกษา',
    'สถาบันอุดมศึกษาอิสระนานาชาติ',
    'สถาบันอุดมศึกษาที่มีศักยภาพสูงจากต่างประเทศ'
    );

    $subCatsExc = array(

    'อดีตสถาบันอุดมศึกษา',
    'โครงการจัดตั้งสถาบันอุดมศึกษา'

    );
    $url = 'https://th.wikipedia.org/wiki/%E0%B8%A3%E0%B8%B2%E0%B8%A2%E0%B8%8A%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%AA%E0%B8%96%E0%B8%B2%E0%B8%9A%E0%B8%B1%E0%B8%99%E0%B8%AD%E0%B8%B8%E0%B8%94%E0%B8%A1%E0%B8%A8%E0%B8%B6%E0%B8%81%E0%B8%A9%E0%B8%B2%E0%B9%83%E0%B8%99%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B9%80%E0%B8%97%E0%B8%A8%E0%B9%84%E0%B8%97%E0%B8%A2';

    $keyCat = function ($title) {
        return preg_replace('/\[.*.\]+$/u', '', trim($title));
    };

    $doc = new DOMDocument();
    $doc->loadHTMLFile($url);

    $finder = new DomXPath($doc);
    $classname="mw-parser-output";

    $university = array();


    $elements = $finder->query("//div[@class='mw-parser-output']");


    if (!is_null($elements)) {
        foreach ($elements as $element) {
            $nodes = $element->childNodes;
            foreach ($nodes as $node) {
                if (($node->nodeName === "h2") && (in_array($keyCat($node->nodeValue), $uniCats))) {
                    $catKey = $keyCat($node->nodeValue);
                    if (!isset($university[$catKey])) {
                        $university[$catKey] = array();
                    }
                }
                if (($node->nodeName === "h3") && (in_array($keyCat($node->nodeValue), $subCats))) {
                    $subKey = $keyCat($node->nodeValue);
                    if (!isset($university[$catKey][$subKey])) {
                        $university[$catKey][$subKey] = array();
                    }
                }


                if ($node->nodeName === "table") {
                    $table = $node->childNodes;
                    $tidx = 0;
                    $uniLine = array();
                    foreach ($table as $tEl) {
                        if ($tidx == 0) {
                            if (in_array($keyCat($tEl->nodeValue), $subCatsExc)) {
                                break 1;
                            }
                        } elseif ($tidx != 1) {
                            $tr = $tEl->childNodes;
                            $tdidx = 0;

                            foreach ($tr as $td) {
                                switch ($tdidx) {
          case "2":
            $name = localeArray($keyCat($td->nodeValue));
            break;

          case "4":
            $acronym = localeArray(explode('/', $keyCat($td->nodeValue)));
            break;

          case "8":
            $founded = $keyCat($td->nodeValue);
            break;

          case "10":
            $location = localeArray($keyCat($td->nodeValue));
            break;

         } //switch
                                $tdidx++;
                            } // foreach
                            $uniLine[] = array(
            'name' => $name,
            'acronym' => $acronym,
            'founded' => $founded,
            'location' => $location,
        );
                        } //else
                        $tidx++;
                    } // foreach
                    if (isset($subKey)) {
                        $university[$catKey][$subKey] = array_merge($university[$catKey][$subKey], $uniLine);
                    }
                } // if
            }  //foreach
        } // foreach
    } // if

return ($type === 'json') ? json_encode($university, JSON_PRETTY_PRINT|JSON_NUMERIC_CHECK|JSON_UNESCAPED_UNICODE):$university;
}
